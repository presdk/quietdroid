package jid.quitedroid.Receivers;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.CalendarContract;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import jid.quitedroid.Database.DatabaseOperations;
import jid.quitedroid.Database.EventOperations;
import jid.quitedroid.Database.EventScheduler;
import jid.quitedroid.Database.TableData;
import jid.quitedroid.GlobalConstants;
import jid.quitedroid.Modes.Mode;
import jid.quitedroid.Services.BackgroundService;

/**
 * Created by Dawin on 6/18/2016.
 */
public class CalendarProcessor extends BroadcastReceiver {

    //Instance Table query arguments array
    public static String[] INSTANCE_TABLE_REQUESTING_ARGS = new String[]{
            CalendarContract.Instances.BEGIN,
            CalendarContract.Instances.END,
            CalendarContract.Instances.TITLE,
            CalendarContract.Instances._ID,
            CalendarContract.Instances.ALL_DAY,
            CalendarContract.Instances.EVENT_ID,
            CalendarContract.Instances.RRULE,
    };

    //Instances Table query arguments array index
    public static final int
            INSTANCES_BEGIN = 0,
            INSTANCES_END = 1,
            INSTANCES_TITLE = 2,
            INSTANCES_ID = 3,
            INSTANCES_ALL_DAY = 4,
            INSTANCES_EVENT_ID = 5,
            INSTANCES_RRULE = 6;

    //Events Table query arguments array
    public static final String[] CALENDAR_PROJECTION_DIRTY = new String[]{
            CalendarContract.Events.TITLE,
            CalendarContract.Events.DIRTY,
            CalendarContract.Events.DELETED,
            CalendarContract.Events._ID,
            CalendarContract.Events.DTSTART,
            CalendarContract.Events.DTEND,
            CalendarContract.Events.DURATION
    };

    //Events Table query arguments array index
    public static final int EVENTS_TITLE = 0,
            EVENTS_DIRTY = 1,
            EVENTS_DELETED = 2,
            EVENTS_ID = 3,
            EVENTS_DTSTART = 4,
            EVENTS_DTEND = 5,
            EVENTS_DURATION = 6;
    /** Actions that trigger this receiver **/

    //action_initial_trigger = when the app is first enabled
    public static final String ACTION_INITIAL_TRIGGER = "ACTION_INITIAL_TRIGGER";
    //action_weekly_trigger = when monday is reached to process the new week
    public static final String ACTION_WEEKLY_TRIGGER = "ACTION_WEEKLY_TRIGGER";

    //Return todays date at 0000 hours
    public static Calendar getTodayDateOnly() {

        Calendar todayCalendar = Calendar.getInstance();
        todayCalendar.set(Calendar.HOUR_OF_DAY, 0);
        todayCalendar.set(Calendar.MINUTE, 0);
        todayCalendar.set(Calendar.SECOND, 0);

        return todayCalendar;
    }

    //Calculate the following sunday from input date and return its Calendar
    public static Calendar getThisSunday(Calendar today) {
        Calendar sunday = (Calendar) today.clone();
        //set date to sunday midnight
        sunday.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        sunday.add(Calendar.HOUR_OF_DAY, 23);
        sunday.add(Calendar.MINUTE, 59);
        sunday.add(Calendar.SECOND, 59);

        if (sunday.compareTo(today) == -1)
            //add a week if today is a sunday
            sunday.add(Calendar.DAY_OF_MONTH, 7);

        return sunday;
    }

    /*
    *
    * * Process the events for this week and schedule modes
    *
    * */
    public static void processWeek(Context context) {
        //reset the scheduled events for reprocessing
        cancelAllScheduledModes(context);

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            //stop the process if the user hasn't granted calendar permissions
            return;
        }

        //the condition that there are no events that are going on at current time
        boolean noEventsExistInCurrentTime = true;
        //number of events that were processed
        int numberOfEvents = 0;

        /** Fetch calendar events from today and until sunday **/
        Calendar today = getTodayApprox();

        Cursor cursor = CalendarContract.Instances.query(context.getContentResolver(),
                INSTANCE_TABLE_REQUESTING_ARGS,
                today.getTimeInMillis(),
                getThisSunday(getTodayDateOnly()).getTimeInMillis());

        //Loop that iterates through all events that are fetched
        PROCESS_CALENDAR : {
            if (cursor == null) {
                //stop iterating as there are no events in that time range
                break PROCESS_CALENDAR;
            } else {
                //events are grouped according to overlaps
                int numberOfEventGroups = 0;

                //previous event's start and end time registers
                long previousCursorDtStart = 0, previousCursorDtEnd = 0;
                //current event's start and end time registers
                long dtstart = 0, dtend = 0;

                //open internal sqlite database
                DatabaseOperations database = new DatabaseOperations(context);

                //clean all contents of raw and organised tables to allow reprocessing
                try {
                    database.deleteTableData(database, TableData.TableInfo.TABLE_NAME_EXTRACTED_EVENTS);
                    database.deleteTableData(database, TableData.TableInfo.TABLE_NAME_SCHEDULED_EVENTS);
                } catch (Exception ex) {
                }

                if (!cursor.moveToFirst()) {
                    //stop processing if no events were fetched
                    break PROCESS_CALENDAR;
                }

                do {
                    //check if current cursor overlaps with previous cursor
                    Integer compare =  compareStringTimeInMillis(cursor.getString(INSTANCES_END), System.currentTimeMillis());
                    //iterate to next fetched event if event is after now
                    if(compare == null){
                        //long parse error (this should not happen)
                        continue;
                    }else if(compare == 1){
                        continue;
                    }

                    if (cursor.getString(INSTANCES_ALL_DAY).equals("1")) {
                        //disregard all day events and iterate to next fetched event
                        continue;
                    }

                    /*********************************************************************

                     FROM HERE THE EVENT TO PROCESS IS NOT ALLDAY AND CLOSE TO CURRENT TIME

                     *********************************************************************/

                    try {
                        dtstart = Long.parseLong(cursor.getString(INSTANCES_BEGIN));
                        dtend = Long.parseLong(cursor.getString(INSTANCES_END));
                    } catch (Exception e) {
                        continue;
                    }

                    String title = cursor.getString(INSTANCES_TITLE);

                    /** Check with forced events **/
                    int forcedMode = database.isEventInForcedDb(title, cursor.getString(INSTANCES_ID));
                    //If event is found in FORCED db table, set forced mode to current mode
                    int mode = (forcedMode == -1) ? Mode.getMode(title) : forcedMode;

                    if(mode > 0) {
                        //mode has priority above default mode
                        if(dtstart <= Calendar.getInstance().getTimeInMillis()
                                && dtend >= Calendar.getInstance().getTimeInMillis()){
                            //event exists in current time
                            noEventsExistInCurrentTime = false;
                        }
                    }

                    //colour of evaluated mode
                    int modeColor = Mode.getModeColor(mode);

                    /** CHANGING the COLOR Of events in the calendar **/
                    ContentValues changeColor = new ContentValues();
                    changeColor.put(CalendarContract.Events.EVENT_COLOR, modeColor);

                    Uri updateUri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, Long.parseLong(cursor.getString(INSTANCES_EVENT_ID)));
                    //update the calendar using the content resolver
                    int rows = context.getContentResolver().update(updateUri, changeColor, null, null);

                    /** Evaluate Overlapping events
                     * if previous event and current event doesn't overlap, then give a different group id
                     * overlapping events have the same group id
                     **/
                    if (!isOverlapping(previousCursorDtStart,
                            previousCursorDtEnd,
                            dtstart)) {
                        //no overlap detected between events, increment the number of groups there are
                        numberOfEventGroups++;
                    }

                    //evaluate the recurrance rule to show user
                    String RRULE_MSG = (cursor.getString(INSTANCES_RRULE) == null) ? "Normal Event" : "Recurring Event";

                    //insert each event detail into the database raw table
                    database.insert(database,
                            TableData.TableInfo.TABLE_NAME_EXTRACTED_EVENTS,
                            title,
                            dtstart,
                            dtend,
                            mode,
                            numberOfEventGroups,
                            cursor.getString(INSTANCES_ID),
                            RRULE_MSG
                    );

                    //set previous dtstart and end with current as loop has ended
                    previousCursorDtStart = dtstart;
                    previousCursorDtEnd = dtend;

                    //increment the number of events that have been evaluated
                    numberOfEvents++;
                } while (cursor.moveToNext());

                //close the database cursor when finished
                cursor.close();

                /** Events scheduling **/
                if (numberOfEvents > 0) {
                    //there are events to be scheduled

                    /**
                     *
                     BELOW STATEMENT COMMENTED OUT. USED FOR DEBUGGING. SHOWS THE EXTRACTED EVENTS TABLE ON THE LOGCAT
                     *
                     */
                    //database.showInformation(TableData.TableInfo.TABLE_NAME_EXTRACTED_EVENTS);

                    /** Process the extracted events and insert to the scheduleable events table **/
                    EventOperations eventOperations = new EventOperations();
                    //perform overlapping algorithm using event group ids
                    eventOperations.organizeEventsToNewDbTable();

                    /**
                     *
                     BELOW STATEMENT COMMENTED OUT. USED FOR DEBUGGING. SHOWS THE SCHEDULED EVENTS TABLE ON THE LOGCAT.
                     *
                     */
                    //database.showInformation(TableData.TableInfo.TABLE_NAME_SCHEDULED_EVENTS);

                    /** Schedule the scheduleable events **/
                    EventScheduler eventScheduler = new EventScheduler();
                    eventScheduler.scheduleEvents(context);
                }
                //finish with using the sqlite database
                database.close();
            }
        }
        /** Set mode for NOW **/
        if(noEventsExistInCurrentTime == true){
            //no events were in current time, should be set to default mode now
            AlarmManager am = (AlarmManager)context.getApplicationContext().getSystemService(Context.ALARM_SERVICE);
            PendingIntent setToNormal = EventScheduler.createNewPendingIntent(context, 0, "initialMode", "");
            am.set(AlarmManager.RTC, System.currentTimeMillis(), setToNormal);
        }
    }

    //returns true if the target long is between the lower and upper limits
    private static boolean isOverlapping(long lower, long upper, long target) {
        return target >= lower && target <= upper;
    }

    //Returns true if user changed the calendar
    public static boolean calendarIsDirty(Context context) {


        if (ActivityCompat.checkSelfPermission(context.getApplicationContext(), Manifest.permission.READ_CALENDAR)
                != PackageManager.PERMISSION_GRANTED) {
            //user has not allowed calendar read permissions, abort checking
            return false;
        }
        ContentResolver cr = context.getApplicationContext().getContentResolver();

        //Fetch dirty events
        Cursor cursor = cr.query(CalendarContract.Events.CONTENT_URI,
                CALENDAR_PROJECTION_DIRTY,
                CalendarContract.Events.DTSTART + " >= " + getTodayDateOnly().getTimeInMillis()
                        + " AND "
                        + CalendarContract.Events.DTSTART + " <= " + getThisSunday(getTodayDateOnly()).getTimeInMillis()
                        + " AND ("
                        + CalendarContract.Events.DIRTY + " = 1"
                        + " OR "
                        + CalendarContract.Events.DELETED + " = 1)",
                null, null);

        if(cursor == null)
        {
            //no dirty events
            return false;
        }
        if(!cursor.moveToFirst()){
            //empty events
            return false;
        }

        //Open database
        DatabaseOperations databaseOperations = new DatabaseOperations(context);

        do{
            Integer compare =  compareStringTimeInMillis(cursor.getString(EVENTS_DTEND), System.currentTimeMillis());
            if(compare == null){
                return false;
            }else if(compare == 1){
                //do not process events that end after now
                continue;
            }
            //event is recurring
            boolean isRecurringEvent = cursor.getString(EVENTS_DTEND) == null;

            if(cursor.getString(EVENTS_DELETED).equals("1")) {
                //Event was deleted
                if (databaseOperations.isEventInExtractedTable(cursor.getString(EVENTS_TITLE),
                        cursor.getString(EVENTS_DTSTART),
                        cursor.getString(EVENTS_DTEND),
                        isRecurringEvent)) {
                    //Event was removed from extracted events,
                    // hence there was a change to the calendar
                    return true;
                }
                //deleted event has been evaluated, skip to next event
                continue;
            }

            if (!databaseOperations.isEventInExtractedTable(cursor.getString(EVENTS_TITLE),
                    cursor.getString(EVENTS_DTSTART),
                    cursor.getString(EVENTS_DTEND),
                    isRecurringEvent)) {
                //For debugging use below function
                //databaseOperations.showInformation(TableData.TableInfo.TABLE_NAME_EXTRACTED_EVENTS);

                //this event was added or changed as it cannot be found in the database,
                // hence there was a change in the calendar
                return true;
            }

        }while(cursor.moveToNext());

        //there was no change in the calendar
        return false;
    }

    @Nullable
    // Compares two input longs
    private static Integer compareStringTimeInMillis(Object targetTime, Object now) {
        //Returns -1 if target is before now
        //Returns 0 if target is equal to now
        //Returns 1 if target is after now
        try {
            long t = Long.parseLong(targetTime.toString()),
                    n = Long.parseLong(now.toString());
            if(n < t)
                return -1;
            else if(n == t)
                return 0;
            else
                return 1;
        }catch(Exception e){
            return null;
        }
    }

    //Schedule a weekly trigger for calendar processing
    public static void scheduleTriggerForNextMonday(Context context) {
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        //today at 00:00
        Calendar todayCalendar = Calendar.getInstance();
        todayCalendar.set(Calendar.HOUR_OF_DAY, 0);
        todayCalendar.set(Calendar.MINUTE, 0);
        todayCalendar.set(Calendar.SECOND, 0);

        if ((todayCalendar.get(Calendar.DAY_OF_WEEK)) == Calendar.MONDAY) {
            //today is a monday so schedule a trigger for next week (today + 7 days)
            todayCalendar.add(Calendar.DAY_OF_WEEK, 7);
        }

        /** Schedule the weekly trigger action **/
        Intent intent = new Intent(context, CalendarProcessor.class);
        intent.setAction(ACTION_WEEKLY_TRIGGER);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 1, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        am.set(AlarmManager.RTC, getThisSunday(getTodayDateOnly()).getTimeInMillis(), pendingIntent);
    }

    //Reset all scheduled modes
    public static void cancelAllScheduledModes(Context context) {
        DatabaseOperations databaseOperations = new DatabaseOperations(context);
        List<String> eventIds = databaseOperations.getAllEventIds(TableData.TableInfo.TABLE_NAME_SCHEDULED_EVENTS);
        //TODO cancel all alarms according to elements in the above array
        Iterator<String> eventIdsIterator = eventIds.iterator();
        Intent intent = new Intent(context, SetModeReceiver.class);
        PendingIntent pendingIntent = null;

        while (eventIdsIterator.hasNext()) {
            String eventId = eventIdsIterator.next();
            intent.setData(Uri.parse(eventId));
            pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            am.cancel(pendingIntent);
//            Log.d("CalendarProcessor.Java", "cancelAllScheduledModes: " + eventId);
        }
    }

    public static Calendar getTodayApprox() {
        Calendar today = Calendar.getInstance();
        //in case of a slow 'calendar dirty trigger' process from 20 mins before to catch events that are right NOW
        today.add(Calendar.MINUTE, -20);
        return today;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        //user preferences
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        //quitedroid is enabled
        boolean quiteDroid_isEnabled = sharedPreferences.getBoolean(GlobalConstants.QUITEDROID_ENABLED, false);

        //Open database tables
        DatabaseOperations database = new DatabaseOperations(context);

        if(quiteDroid_isEnabled){
            switch (intent.getAction()) {
                //first installation processing
                case ACTION_INITIAL_TRIGGER:
                    processWeek(context);
                    scheduleTriggerForNextMonday(context);
                    break;

                //PHONE REBOOTED PROCESSING
                //ALARMMANAGER IS RESET, NEEDS RESCHEDULING FROM SCRATCH
                case Intent.ACTION_BOOT_COMPLETED:
                    context.startService(new Intent(context, BackgroundService.class));
                    processWeek(context);
                    scheduleTriggerForNextMonday(context);
                    break;
                //calendar dirty processing
                case Intent.ACTION_PROVIDER_CHANGED: //TODO find an action for calendar changed
                    if(calendarIsDirty(context))
                    {
                        processWeek(context);
                    }
                    break;
                //monday trigger weekly processing
                case ACTION_WEEKLY_TRIGGER:
                    //Delete Forced database on weekly trigger
                    database.cleanForcedDb();
                    processWeek(context);
                    scheduleTriggerForNextMonday(context);
                    break;
                //EXCEPTIONAL CASE, DO NOTHING
                default:
                    break;
            }
        } else {
            //quitedroid is disabled, do not process
        }
    }
}
