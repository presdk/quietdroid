package jid.quitedroid.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.Map;

/**
 * Created by JiYeon on 2016-07-27.
 */
public class SetFeatureReceiver extends BroadcastReceiver{
    private static final String PriorityCallerList = "PriorityCallerList";
    private static final String ExceptionList = "ExceptionList" ;
    private SharedPreferences priorityCallerSharedPreferences, exceptionListSharedPreferences;

    @Override
    public void onReceive(Context context, Intent intent) {
//        Log.d("Priority", "onReceive: ");

        //Retrieve priority list and exception list
        priorityCallerSharedPreferences = context.getSharedPreferences(PriorityCallerList, Context.MODE_PRIVATE);
        exceptionListSharedPreferences = context.getSharedPreferences(ExceptionList, Context.MODE_PRIVATE);

        //Reset exception list
        resetExceptionList();
        //Reset priority list
        resetPriorityList();

//        Log.d("SetFeatureReceiver", "onReceive finished ");
    }

    //Delete the priority callers (Added to exception list by calling more than three times in one event)
    private void resetExceptionList(){
        SharedPreferences.Editor editor = exceptionListSharedPreferences.edit();
        Map<String,?> priorityCallerSharedPreferencesAll = priorityCallerSharedPreferences.getAll();
        Map<String,?> exceptionListSharedPreferencesAll = exceptionListSharedPreferences.getAll();

        for(Map.Entry<String,?> priorityEntry : priorityCallerSharedPreferencesAll.entrySet()){
            for(Map.Entry<String,?> exceptionList : exceptionListSharedPreferencesAll.entrySet()){
                if(priorityEntry.getValue().equals(999)){
                    if(priorityEntry.getKey().equals(exceptionList.getValue())){
//                        Log.d("DELETED",exceptionList.getKey());
                        editor.remove(exceptionList.getKey());
                    }
                }
            }
        }

        //Commit
        editor.commit();
    }

    //Clear all the content of priority list
    private void resetPriorityList(){
        SharedPreferences.Editor editor = priorityCallerSharedPreferences.edit();
        editor.clear();
        editor.commit();
    }
}
