package jid.quitedroid.Modes;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.os.Build;

import jid.quitedroid.Handlers.ContextHandler;
import jid.quitedroid.Features.PriorityCallHandler;
import jid.quitedroid.GlobalConstants;
import jid.quitedroid.Activity.MainActivity;
import jid.quitedroid.R;
import jid.quitedroid.Receivers.SetModeReceiver;
import jid.quitedroid.Handlers.ModeHandler;
import jid.quitedroid.Services.WidgetService;
import jid.quitedroid.Handlers.SoundHandler;

/**
 * Created by Dawin on 4/14/2016.
 */
public class Mode implements IMode {
    public static final int notificationID = 0;
    public static final int NORMAL_MODE = 0, MEETING_MODE = 1, BLOCKING_MODE = 2;

    private int icon;
    private String name;
    private String soundType;
    private String eventTitle;
    public int COLOR;
    protected Notification notification;

    private Context context;
    private SoundHandler soundHandler = new SoundHandler();
    private ModeHandler modeHandler = new ModeHandler();
    private PriorityCallHandler priorityCallHandler = new PriorityCallHandler(ContextHandler.getContext());
    public boolean isInvokedByWidget = false;

    public Mode(Context context, int icon, String name, String soundType, String eventTitle, int color) {
        this.context = context;
        this.icon = icon;
        this.name = name;
        this.soundType = soundType;
        this.eventTitle = eventTitle;
        this.COLOR = color;
    }

    public String toString(){
        return name;
    }

    @Override
    public void invokeMode() {
        //Check if widget mode is on
        //if it is then don't invoke new mode
        if(ModeHandler.isMyServiceRunning(ContextHandler.getContext(), WidgetService.class)){
            if(isInvokedByWidget == false){
                return;
            }
        }

        //Build new notification
        Notification newNotification = getMyActivityNotification(context, name, icon);
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(GlobalConstants.UNIQUE_QUITE_DROID_NOTIFICATION_ID, newNotification);

        modeHandler.setMode(this.name);
        modeHandler.setEventTitle(this.eventTitle);
        //Clear priority call list
        priorityCallHandler.resetCallerList();

        //Set the mode sound level
        setSoundType();
    }

    public Notification getMyActivityNotification(Context context, String mode, int icon){
        PendingIntent startActivity = PendingIntent.getActivity(
                context,
                0,
                new Intent(context, MainActivity.class),
                PendingIntent.FLAG_CANCEL_CURRENT
        );

        Notification.Builder builder = new Notification.Builder(context)
                .setContentTitle("QuietDroid")
                .setContentText(mode)
                .setContentInfo(isInvokedByWidget? "Widget" : "Calendar")
                .setSmallIcon(icon)
                .setContentIntent(startActivity)
                .setOngoing(true)
                .setAutoCancel(false);

        /*  FOR STOP ACTION BUTTON */
        Intent stopModeIntent = new Intent(context, SetModeReceiver.class);
        stopModeIntent.setAction(isInvokedByWidget ? SetModeReceiver.ACTION_STOP_MODE_WIDGET : SetModeReceiver.ACTION_STOP_MODE_CALENDAR);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, stopModeIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        Notification.Action stopModeAction = null;
        //for android versions above and == to M
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            stopModeAction = new Notification.Action.Builder(
                    Icon.createWithResource(context, R.drawable.icon_stop),
                    "Stop",
                    pendingIntent).build();
            builder.addAction(stopModeAction);
            //for older android versions support


        }else if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1){
        //for versions below and equal to 5.1
            builder.addAction(R.drawable.icon_stop,
                    "Stop",
                    pendingIntent).build();
        }

        return builder.build();

    }

    public void setSoundType() {
        soundHandler.setSoundMode(this.soundType);
    }

    public void deactivate() {
//        MainActivity.manager.cancel(notificationID);

    }

    public static int getMode(String title){
        if(SilentMode.isModeKeyword(title)){
            return BLOCKING_MODE; //TODO Tell Dawin that this error has been fixed :)!
        }else if(VibrateMode.isModeKeyword(title)){
            return MEETING_MODE;
        }else{
            return NORMAL_MODE;
        }
    }

    public static int getModeColor(int mode) {
        if(mode == MEETING_MODE){
            return VibrateMode.COLOR;
        }else if(mode == BLOCKING_MODE){
            return SilentMode.COLOR;
        }else{
            return DefaultMode.COLOR;
        }
    }



    public void forceInvokeMode() {
        //Build new notification
        Notification newNotification = getMyActivityNotification(context, name, icon);
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(GlobalConstants.UNIQUE_QUITE_DROID_NOTIFICATION_ID, newNotification);
        modeHandler.setMode(this.name);
        modeHandler.setEventTitle(this.eventTitle);
        //Clear priority call list
        priorityCallHandler.resetCallerList();

        //Set the mode sound level
        setSoundType();
    }
}
