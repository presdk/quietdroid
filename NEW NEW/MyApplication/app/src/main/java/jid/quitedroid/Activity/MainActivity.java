package jid.quitedroid.Activity;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.CalendarContract;
import android.provider.CallLog;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import jid.quitedroid.Database.DatabaseOperations;
import jid.quitedroid.Database.TableData;
import jid.quitedroid.Features.ForceModeHandler;
import jid.quitedroid.GlobalConstants;
import jid.quitedroid.Handlers.ContextHandler;
import jid.quitedroid.Modes.SilentMode;
import jid.quitedroid.Modes.VibrateMode;
import jid.quitedroid.Modes.Mode;
import jid.quitedroid.Modes.DefaultMode;
import jid.quitedroid.Interface.MyAnimation;
import jid.quitedroid.R;
import jid.quitedroid.Receivers.CalendarProcessor;
import jid.quitedroid.Handlers.ModeHandler;
import jid.quitedroid.Services.WidgetService;
import jid.quitedroid.Interface.ShowcaseViews;


public class MainActivity extends AppCompatActivity implements GestureDetector.OnGestureListener{
    private static final int MAX_NUM_TABS = 3;
    //receiver that updates the user interface
    private BroadcastReceiver uiUpdateReceiver;

    public static NotificationManager manager;
    //context of application
    public static Context appContext;
    //Mode handler that executes the mode
    public ModeHandler modeHandler;
    //User preferences
    public SharedPreferences sharedPreferences;
    //calendar permissions unique id that is used for checking permissions
    public static final int calendar_permissions = 0;

    /** UI Views and components **/
    TextView tvCurrentModeDetails;
    TabHost host;
    int previousTabIndex;
    int currentTabIndex;
    View previousView;
    View currentView;
    Switch enable_quitedroid;
    TextView blockedCallDetails;

    //Handles force modes in event tab
    ForceModeHandler forceModeHandler;
    // Intermediate class for android alarm services
    AlarmManager am;
    //Gester Detector that is compatible with touch events
    GestureDetectorCompat gestureDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //initialise high level variables
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        am = (AlarmManager)getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        appContext = getApplicationContext();
        modeHandler = new ModeHandler();
        gestureDetector = new GestureDetectorCompat(this, this);

        //register the ui refresh broadcast receiver
        uiUpdateReceiver = createBroadcastReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(uiUpdateReceiver, new IntentFilter(GlobalConstants.FILTER_MAINUI_REFRESH));

        tvCurrentModeDetails = (TextView)findViewById(R.id.tvCurrentModeDetails);

        if(!userAllowedAllPermissions()){
            SetQDEnabledInDB(false);
        }


        //initialise the enable quitedroid switch
        InitialiseSwitch(IsQuietDroidEnabled());

        //initialise tab host
        InitialiseTabs();
        //initialise events tab
        InitialiseEventsTab();

        /** Initialise UI **/
        InitialiseLegendHelpBar();
        //refresh UI, to refresh icons, textviews etc...
        refreshUI();
        //refresh call log
        refreshCallLog();
        //Initialise Button onclick events
        InitialiseButtons();

        if(IsQuietDroidEnabled()){
            //reprocess calendar if phone was booted and quitedroid is enabled
            CalendarProcessor.processWeek(this);
        }

        SharedPreferences mSharedPref = PreferenceManager.getDefaultSharedPreferences(ContextHandler.getContext());
        if(mSharedPref.getBoolean(GlobalConstants.FIRST_BOOT_MAIN, true))
        {
            //Start the tutorial guide if app was booted for the first time
            ShowcaseViews shows = new ShowcaseViews(this);
            shows.show();
        }
    }
    //returns if quitedroid is enabled
    private boolean IsQuietDroidEnabled() {
        return sharedPreferences.getBoolean(GlobalConstants.QUITEDROID_ENABLED, false);
    }

    //initialise the events tab
    private void InitialiseEventsTab() {
        forceModeHandler = new ForceModeHandler(this);
        forceModeHandler.setOnTouchListener(GetOnTouchListener());
    }

    //initialise the legend in the events tab
    private void InitialiseLegendHelpBar() {
        LinearLayout helpBarBlocking = (LinearLayout)findViewById(R.id.helpBarBlocking);
        LinearLayout helpBarFormal = (LinearLayout)findViewById(R.id.helpBarFormal);
        LinearLayout helpBarNormal = (LinearLayout)findViewById(R.id.helpBarNormal);

        helpBarBlocking.setOnClickListener(GetOnClickShowHelp(SilentMode.name));
        helpBarFormal.setOnClickListener(GetOnClickShowHelp(VibrateMode.name));
        helpBarNormal.setOnClickListener(GetOnClickShowHelp(DefaultMode.name));
    }

    //Show dialogs when legend is clicked
    private View.OnClickListener GetOnClickShowHelp(final String mode){
        return new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                switch(mode){
                    case SilentMode.name:
                        SettingsActivity.displayHelpMessage(mode, GlobalConstants.LEGEND_BLOCKING_MSG, MainActivity.this, R.drawable.icon_red);
                        break;
                    case VibrateMode.name:
                        SettingsActivity.displayHelpMessage(mode, GlobalConstants.LEGEND_FORMAL_MSG, MainActivity.this, R.drawable.icon_orange);
                        break;
                    case DefaultMode.name:
                        SettingsActivity.displayHelpMessage(mode, GlobalConstants.LEGEND_NORMAL_MSG, MainActivity.this, R.drawable.icon_green);
                }

            }
        };
    }

    //initialise the enable switch in home page
    private void InitialiseSwitch(boolean isEnabled) {
        enable_quitedroid = (Switch)findViewById(R.id.sw_activate_quitedroid);


        enable_quitedroid.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                enableSwitchOnCheckedChanged(isChecked);

            }
        });
        enableSwitchOnCheckedChanged(isEnabled);
    }

    private void enableSwitchOnCheckedChanged(boolean isChecked) {
        //check version and if needs permissions
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!userAllowedAllPermissions()) {
                requestAllRequiredPermissions();
                return;
            }
        }

        if (isChecked) {
            //start quitedroid
            EnableQuiteDroid();
        }else{
            //end quitedroid
            DisableQuiteDroid();
        }
        //Write the enabled status to database
        SetQDEnabledInDB(isChecked);
        enable_quitedroid.setChecked(isChecked);
        //update the home page UI
        refreshUI();
    }

    //Write quitedroid enable status to preferences
    private void SetQDEnabledInDB(boolean isEnabled) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(GlobalConstants.QUITEDROID_ENABLED, isEnabled);
        editor.apply();
    }

    //Setup Buttons
    private void InitialiseButtons() {
        //Link to system call log so the user can perform call back / send message
        ImageButton buttonCallLog = (ImageButton) findViewById(R.id.buttonCallLog);
        buttonCallLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Let's test something!
                Intent resultIntent = new Intent();
                resultIntent.setAction(Intent.ACTION_VIEW);
                resultIntent.setType(CallLog.Calls.CONTENT_TYPE);
                startActivity(resultIntent);
            }
        });

        //clear call log that is displayed on the main activity
        ImageButton buttonClear = (ImageButton) findViewById(R.id.buttonClear);
        buttonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatabaseOperations databaseOperations = new DatabaseOperations(appContext.getApplicationContext());
                databaseOperations.deleteTableData(databaseOperations, TableData.TableInfo.TABLE_NAME_BLOCKED_CALL_LOG);
                refreshCallLog();
            }
        });

        /* Quick Add Handler */
        forceModeHandler.setOnTouchListener(GetOnTouchListener());
        forceModeHandler.displayNormalModeEvents(this);

        final ImageButton buttonRefreshQuickAdd = (ImageButton) findViewById(R.id.quickAddRefreshButton);
        buttonRefreshQuickAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Refreshed events!", Toast.LENGTH_LONG).show();
                forceModeHandler.setOnTouchListener(GetOnTouchListener());
                forceModeHandler.displayNormalModeEvents(MainActivity.this);
            }
        });
        TextView lblRefreshButton = (TextView)findViewById(R.id.lblRefreshEvents);
        lblRefreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonRefreshQuickAdd.callOnClick();
            }
        });

        final ImageButton buttonGoogleCalendar = (ImageButton) findViewById(R.id.googleCalendarButton);
        buttonGoogleCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Open Google Calendar
                Uri.Builder builder = CalendarContract.CONTENT_URI.buildUpon();
                builder.appendPath("time");
                ContentUris.appendId(builder, System.currentTimeMillis());
                Intent intent = new Intent(Intent.ACTION_VIEW)
                        .setData(builder.build());
                startActivity(intent);
            }
        });
        TextView lblCalendarButton = (TextView)findViewById(R.id.lblCalendarButton);
        lblCalendarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonGoogleCalendar.callOnClick();
            }
        });

        //Settings
        final ImageButton iconSettings = (ImageButton) findViewById(R.id.iconSettings);
        iconSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, SettingsActivity.class));
            }
        });
    }

    private void DisableQuiteDroid() {
        NotificationManager mNotificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        //remove the quitedroid notification
        mNotificationManager.cancel(GlobalConstants.UNIQUE_QUITE_DROID_NOTIFICATION_ID);
        //cancel the next monday trigger, cancel all scheduled alarms from database
        Intent intent = new Intent(getApplicationContext(), CalendarProcessor.class);
        intent.setAction(CalendarProcessor.ACTION_WEEKLY_TRIGGER);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(MainActivity.this, 1, intent, PendingIntent.FLAG_ONE_SHOT);
        am.cancel(pendingIntent);

        //cancel all scheduled modes
        CalendarProcessor.cancelAllScheduledModes(getApplicationContext());

        //stop the widget service thread
        stopService(new Intent(MainActivity.this, WidgetService.class));
    }

    private void EnableQuiteDroid() {
        //Reprocess quick events
        forceModeHandler.displayNormalModeEvents(MainActivity.this);

        //Reprocess Calendar
        Intent calendarProcessIntent = new Intent(MainActivity.this, CalendarProcessor.class);
        calendarProcessIntent.setAction(CalendarProcessor.ACTION_INITIAL_TRIGGER);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(MainActivity.this, 1, calendarProcessIntent, PendingIntent.FLAG_ONE_SHOT);
        am.set(AlarmManager.RTC, System.currentTimeMillis(), pendingIntent);
    }

    //Populate the call log tab contents
    private void refreshCallLog() {
        DatabaseOperations databaseOperations = new DatabaseOperations(appContext.getApplicationContext());
        blockedCallDetails = (TextView) findViewById(R.id.missedCallTextView);
        TextView textViewNoCalls = (TextView)findViewById(R.id.tvNoCalls);
        if(!databaseOperations.createCallLog().isEmpty()){
            //call log is not empty, show the log
            blockedCallDetails.setText(databaseOperations.createCallLog());
            textViewNoCalls.setVisibility(View.INVISIBLE);
        }else{
            //call log is empty, show the default "no blocked calls" text
            blockedCallDetails.setText("");
            textViewNoCalls.setVisibility(View.VISIBLE);
        }
        //set the log to be scrollable
        blockedCallDetails.setMovementMethod(new ScrollingMovementMethod());
    }

    //Setup the tabs
    private void InitialiseTabs() {
        host = (TabHost)findViewById(R.id.tabHost);
        host.setup();

        //Call log tab
        TabHost.TabSpec spec = host.newTabSpec("Call Log");
        spec.setContent(R.id.call_tab);
        spec.setIndicator("Call Log");
        host.addTab(spec);

        //Home tab
        spec = host.newTabSpec("Home");
        spec.setContent(R.id.home_tab);
        spec.setIndicator("Home");
        host.addTab(spec);

        //Events tab
        spec = host.newTabSpec("Events");
        spec.setContent(R.id.quick_add_tab);
        spec.setIndicator("Events");
        host.addTab(spec);

        host.setCurrentTab(1);

        previousView = host.getCurrentView();
        previousTabIndex = 1;

        host.setOnTabChangedListener(GetOnTabChanged());
    }

    public View.OnTouchListener GetOnTouchListener(){
        return new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        };
    }



    @Override
    public boolean onTouchEvent(MotionEvent touchEvent){
        return gestureDetector.onTouchEvent(touchEvent);
    }

    //Refresh UI of all tabs
    public void refreshUI() {

        if(sharedPreferences.getBoolean(GlobalConstants.QUITEDROID_ENABLED, false) == true){
            Mode currentMode = new DefaultMode(MainActivity.this, "");
            switch(modeHandler.getMode()){
                case DefaultMode.name:
                    currentMode = new DefaultMode(MainActivity.this, "");
                    break;
                case VibrateMode.name:
                    currentMode = new VibrateMode(MainActivity.this, "");
                    break;
                case SilentMode.name:
                    currentMode = new SilentMode(MainActivity.this, "");
                    break;
            }
            tvCurrentModeDetails.setTextColor(currentMode.COLOR);
            tvCurrentModeDetails.setText(currentMode.toString());
        }else{
            tvCurrentModeDetails.setTextColor(Color.GRAY);
            tvCurrentModeDetails.setText("Deactivated");
        }

        refreshCallLog();
    }

    //Request user permissions required for this app
    public void requestAllRequiredPermissions(){

        ActivityCompat.requestPermissions(this,
                GlobalConstants.requiredPermissions,
                calendar_permissions);
    }

    //Returns true if input permission was granted
    public boolean userAllowedPermission(String permission) {
        return ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED;
    }

    //Returns true if all permissions were granted
    public boolean userAllowedAllPermissions(){
        for(int i = 0; i < GlobalConstants.requiredPermissions.length; i++){
            if(!userAllowedPermission(GlobalConstants.requiredPermissions[i]))
                return false;
        }
        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case calendar_permissions: {
                for(int i = 0; i < grantResults.length; i++){
                    //request specific permissions
                    if((i == 0 || i == 2 || i == 3 || i == 5) && grantResults[i] == PackageManager.PERMISSION_DENIED){
                        //show a dialog to explain why a permission is needed
                        showPermissionsRequiredErrorDialog(GlobalConstants.requiredPermissionsFormatted[i], GlobalConstants.requiredPermissionsResId[i],true);
                    }
                }

                // If request is cancelled, the result arrays are empty.
                boolean isEnabled;
                if (userAllowedAllPermissions()) {
                    isEnabled = true;
                    SetQDEnabledInDB(isEnabled);
                    EnableQuiteDroid();
                }else {
                    isEnabled  = false;
                    SetQDEnabledInDB(isEnabled);
                }
                enable_quitedroid.setChecked(isEnabled);
                refreshUI();

            }
            return;
        }
    }

    private void showPermissionsRequiredErrorDialog(String permission, int message, boolean startSettings){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(permission + " permission needed");
        LayoutInflater inflater = this.getLayoutInflater();
        dialogBuilder.setMessage(message);
        if(startSettings){
            dialogBuilder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    startActivity(intent);
                }
            });
        }
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onDestroy(){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(GlobalConstants.MAINACTIVITY_ALIVE, false);
        if(uiUpdateReceiver != null){
            //unregister the ui update receiver
            LocalBroadcastManager.getInstance(this).unregisterReceiver(uiUpdateReceiver);
        }
        super.onDestroy();
    }

    private BroadcastReceiver createBroadcastReceiver(){
        return new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                refreshUI();
            }
        };
    }

    /******************************
     ** Gesture Related Functions **
     ******************************/

    public TabHost.OnTabChangeListener GetOnTabChanged(){
        return new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
//                Log.d("MainActivity.java", "onTabChanged: invoked");
//                AnimateTabChange("source_tab");
            }
        };
    }

    //Tab change animations
    public void AnimateTabChange(String source){
        switch(source){
            case "source_fling":
                currentView = host.getTabContentView().getChildAt(currentTabIndex);
                previousView = host.getTabContentView().getChildAt(previousTabIndex);
                break;
            case "source_tab":
                currentView = host.getCurrentView();
                previousView = host.getTabContentView().getChildAt(previousTabIndex);
                break;
        }
        if(host.getCurrentTab() > previousTabIndex) {
            previousView.setAnimation(MyAnimation.outToLeftAnimation());
            currentView.setAnimation(MyAnimation.inFromRightAnimation());
        }else if(host.getCurrentTab() < previousTabIndex){
            previousView.setAnimation(MyAnimation.outToRightAnimation());
            currentView.setAnimation(MyAnimation.inFromLeftAnimation());
        }
        previousView = currentView;
        previousTabIndex = host.getCurrentTab();
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        int currentTab = host.getCurrentTab();

        currentTabIndex = currentTab;

        if(Math.abs(velocityY) > Math.abs(velocityX))
            //scroll gesture was detected, allow scrolling do not change tabs
            return false;

        if(velocityX > 0){
            if(currentTab == 0)
                //leftmost tab already reached, do not change tabs
                return false;
            //swipe right
            previousTabIndex = currentTab;
            currentTabIndex = currentTab - 1;
            host.setCurrentTab(currentTab - 1);
        }else if(velocityX < 0){
            if(currentTab == MAX_NUM_TABS - 1)
                //rightmost tab already reached, do not change tabs
                return false;
            //swipe left
            previousTabIndex = currentTab;
            currentTabIndex = currentTab + 1;
            host.setCurrentTab(currentTab + 1);
        }
        AnimateTabChange("source_fling");
        return true;
    }
}
