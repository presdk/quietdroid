package jid.quitedroid.Database;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;

import jid.quitedroid.Modes.SilentMode;
import jid.quitedroid.Modes.VibrateMode;
import jid.quitedroid.Modes.DefaultMode;
import jid.quitedroid.Receivers.SetModeReceiver;

/**
 * Created by JiYeon on 2016-07-18.
 */

//Process all the events in organized table of database
public class EventScheduler {
    private DatabaseOperations databaseOperations;
    private AlarmManager am;

    private static final int
            EVENT_TITLE = 0,
            EVENT_START_TIME = 1,
            EVENT_END_TIME = 2,
            EVENT_MODE = 3,
            GROUP_ID = 4,
            EVENT_ID = 5,
            EVENT_RRULE = 6;

    //When this method is invoked, all the organized events are placed into the Alarm Manager
    public void scheduleEvents(Context context) {
        databaseOperations = new DatabaseOperations(context);
        //Initialize Alarm Manager
        am = (AlarmManager)context.getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        //Create cursor
        Cursor cursor = databaseOperations.getCursor(databaseOperations, TableData.TableInfo.TABLE_NAME_SCHEDULED_EVENTS);
        //Move cursor to first row
        cursor.moveToFirst();
        //Continue while there is a next row to move to
        do{
            //Retrieve new intent, set to Alarm Manager
            try{
                am.set(AlarmManager.RTC, cursor.getLong(EVENT_START_TIME), createNewPendingIntent(context, cursor.getInt(EVENT_MODE), cursor.getString(EVENT_ID), cursor.getString(EVENT_TITLE)));
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }while(cursor.moveToNext());

        cursor.close();
    }

    public static PendingIntent createNewPendingIntent(Context context, int mode, String eventID, String eventTitle){
        Intent d_intent = new Intent(context, SetModeReceiver.class);
        d_intent.setAction(SetModeReceiver.ACTION_INVOKE_MODE);
        d_intent.setData(Uri.parse(eventID));
        d_intent.putExtra("mode", manageMode(mode));
        d_intent.putExtra("title", eventTitle);
        PendingIntent d_pi = PendingIntent.getBroadcast(context, 1, d_intent, PendingIntent.FLAG_ONE_SHOT);
        return d_pi;
    }

    public static String manageMode(int mode){
        switch(mode){
            case 2: //Silent Mode
                return SilentMode.name;
            case 1: //Vibrate Mode
                return VibrateMode.name;
            case 0: //Default Mode
                return DefaultMode.name;
            default: //Non Valid Mode
                return "Testing Non Valid Mode";
        }
    }
}
