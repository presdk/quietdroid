package jid.quitedroid.Handlers;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;

import java.util.Map;

import jid.quitedroid.Handlers.ContextHandler;
import jid.quitedroid.GlobalConstants;
import jid.quitedroid.Modes.VibrateMode;
import jid.quitedroid.Modes.DefaultMode;
import jid.quitedroid.Handlers.SoundHandler;

/**
 * Created by JiYeon on 2016-04-25.
 */
public class ModeHandler {
    private static final String MyPREFERENCES = "InternalMode";
    private SharedPreferences sharedpreferences, defaultpreferences;

    //Constructor
    public ModeHandler() {
        //Shared preferences that store the current internal mode in a xml file
        sharedpreferences = ContextHandler.getContext().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        defaultpreferences = PreferenceManager.getDefaultSharedPreferences(ContextHandler.getContext());
    }

    //Set current mode into the shared preferences
    public void setMode(String mode) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        //Clear internal mode shared preferences
        editor.clear();
        //Put in new mode in mode shared preferences
        editor.putBoolean(mode, true);
        //Commit
        editor.commit();
        //Perform a UI refresh
        broadcastUIRefresh();
    }

    //Revert back to default mode
    public void revertToNormal(){
        DefaultMode defaultMode = new DefaultMode(ContextHandler.getContext(), "");
        //Force start default mode
        defaultMode.forceInvokeMode();
        //Perform UI refresh
        broadcastUIRefresh();
    }

    //Perform UI refresh
    public void broadcastUIRefresh(){
            try {
                LocalBroadcastManager.getInstance(ContextHandler.getContext()).sendBroadcast(new Intent(GlobalConstants.FILTER_MAINUI_REFRESH));
            }catch(Exception e) {
                e.printStackTrace();
            }
    }

    //Check is system service is running
    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public String getMode() {
        //Retrieve current mode
        Map<String, ?> keys = sharedpreferences.getAll();
        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            //Return current mode
            return entry.getKey();
        }
        //Should not reach here!
        return "Default Mode";
    }

    //Set event's title in the shared preferences
    public void setEventTitle(String eventTitle){
        SharedPreferences.Editor editor = defaultpreferences.edit();
        editor.putString(GlobalConstants.EVENT_TITLE, eventTitle);
        editor.commit();
    }

    //Retrieve the event's title from the shared preferences
    public String getEventTitle(){
        return defaultpreferences.getString(GlobalConstants.EVENT_TITLE, "DEBUG INSTANCES_TITLE");
    }
}
