package jid.quitedroid.Receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import jid.quitedroid.Modes.SilentMode;
import jid.quitedroid.Modes.VibrateMode;
import jid.quitedroid.Modes.Mode;
import jid.quitedroid.Modes.DefaultMode;
import jid.quitedroid.Handlers.ModeHandler;
import jid.quitedroid.Services.WidgetService;

/**
 * Created by Dawin on 7/17/2016.
 */
public class SetModeReceiver extends BroadcastReceiver {
    final String TAG = "SetModeReceiver.class";
    public static final String ACTION_INVOKE_MODE = "action_invoke_mode";
    public static final String ACTION_STOP_MODE_WIDGET = "action_stop_mode_widget";
    public static final String ACTION_STOP_MODE_CALENDAR = "action_stop_mode_calendar";


    @Override
    public void onReceive(Context context, Intent intent) {
        switch(intent.getAction()){
            case ACTION_INVOKE_MODE:
                invokeMode(context, intent);
                break;
            case ACTION_STOP_MODE_WIDGET:
                stopMode(context, intent);
                context.stopService(new Intent(context, WidgetService.class));
                AlarmManager am = (AlarmManager)context.getApplicationContext().getSystemService(Context.ALARM_SERVICE);

                Intent processWeek = new Intent(context, CalendarProcessor.class);
                processWeek.setAction(CalendarProcessor.ACTION_INITIAL_TRIGGER);
                am.set(AlarmManager.RTC, System.currentTimeMillis(), PendingIntent.getBroadcast(context, 1, processWeek, PendingIntent.FLAG_CANCEL_CURRENT));

                break;
            case ACTION_STOP_MODE_CALENDAR:
                stopMode(context, intent);
            default:
                //do nothing
        }


    }

    private void invokeMode(Context context, Intent intent) {
        String TAG = "SetModeReceiver";
//        Log.d(TAG, "onReceive: " + intent.getDataString());

        String modeName = intent.getStringExtra("mode");
        String eventTitle = intent.getStringExtra("title");
        Mode mode = null;

        switch (modeName) {
            case SilentMode.name:
                mode = new SilentMode(context, eventTitle);
                break;
            case VibrateMode.name:
                mode = new VibrateMode(context, eventTitle);
                break;
            case DefaultMode.name:
                mode = new DefaultMode(context, eventTitle);
                break;
            default:
                //should not reach this code
                //do nothing
//                Log.d(TAG, "onReceive: " + "modeName is default, something is wrong!!! Name: " + modeName);
                return;
        }
        mode.invokeMode();
    }
    private void stopMode(Context context, Intent intent) {
//        Log.d(TAG, "stopMode: stopped mode");
        ModeHandler modeHandler = new ModeHandler();
        modeHandler.revertToNormal();
    }

}
