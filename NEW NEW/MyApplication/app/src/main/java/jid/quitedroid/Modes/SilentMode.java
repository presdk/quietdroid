package jid.quitedroid.Modes;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jid.quitedroid.Handlers.ContextHandler;
import jid.quitedroid.GlobalConstants;
import jid.quitedroid.R;

/**
 * Created by Dawin on 4/14/2016.
 */
public class SilentMode extends Mode implements IMode{
    public static final int icon = R.drawable.smallblockingmodeicon;
    public static final String name = "Silent Mode";
    public static final String soundType = "silent";
    public static final int COLOR = Color.RED;

    public SilentMode(Context context, String eventTitle){
        super(context, icon, name, soundType, eventTitle, COLOR);
    }

    public static boolean isModeKeyword(String keyword) {
        List<String> keywords = getBlockingModeKeywordList();

        //for normal mode
        if(keywords.size() <= 0) return false;
        keyword = keyword.toLowerCase();
        for (String s : keywords)
            if (keyword.contains(s.toLowerCase()))
                return true;
        return false;
    }

    private static List<String> getBlockingModeKeywordList(){
        SharedPreferences sharedPreferences = ContextHandler.getContext().getSharedPreferences(GlobalConstants.BLOCKING_MODE_PREFERENCES, Context.MODE_PRIVATE);
        //Retrieve list
        Map<String, ?> keys = sharedPreferences.getAll();
        //Set new list
        List<String> list = new ArrayList<String>();

        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            //Add to contact names array list
            list.add(entry.getKey());
        }

        return list;
    }
}