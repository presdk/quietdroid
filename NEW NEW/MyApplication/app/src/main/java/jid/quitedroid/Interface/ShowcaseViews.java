package jid.quitedroid.Interface;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;


import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

import java.util.ArrayList;
import java.util.List;

import jid.quitedroid.GlobalConstants;
import jid.quitedroid.R;

/**
 * This class allows you to have multiple showcases on one screen, you can then page through
 * each one as you press the 'ok' button
 */
public class ShowcaseViews {

    private final List<ShowcaseView.Builder> views = new ArrayList<>();
    private final Activity activity;
    public int index = 0;
    TabHost tabHost;

    SharedPreferences sharedPreferences;

    /**
     * @param activity               The activity containing the views you wish to showcase
     */
    public ShowcaseViews(Activity activity) {
        this.activity = activity;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());
        this.tabHost = (TabHost)activity.findViewById(R.id.tabHost);
    }


    public void setChainClickListener(final ShowcaseView viewTemplate) {
        viewTemplate.overrideButtonClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                index++;
                viewTemplate.hide();
                show();
            }
        });
    }

    private ShowcaseView GetShowCaseView(int view, String message, boolean blockAllTouches){
        ShowcaseView.Builder show = new ShowcaseView.Builder(activity, true)
                .withNewStyleShowcase()
                .setTarget(new ViewTarget(view, activity))
                .setStyle(ShowcaseView.BELOW_SHOWCASE)
                .setContentText(message);

        if(blockAllTouches)
            show.blockAllTouches();
        else
            show.doNotBlockTouches();
        ShowcaseView showCase = show.build();
        setChainClickListener(showCase);
        return showCase;
    }
    private ShowcaseView GetShowCaseView(View view, String message, boolean blockAllTouches){
        ShowcaseView.Builder show = new ShowcaseView.Builder(activity, true)
                .withNewStyleShowcase()
                .setTarget(new ViewTarget(view))
                .setStyle(ShowcaseView.BELOW_SHOWCASE)
                .setContentText(message);
        if(blockAllTouches)
            show.blockAllTouches();
        else
            show.doNotBlockTouches();
        ShowcaseView showCase = show.build();
        setChainClickListener(showCase);
        return showCase;
    }
    /**
     * Showcases will be shown in the order they where added, continuing when the button is pressed
     */
    public void show() {
        //Took me ages to figure out that you need to dynamically make a whole new showcase in order for it to work, what a weird issue
        //it's only open source so it isn't perfect, guess we have to manage with what we have
        switch (index){
            case 0:
                GetShowCaseView(R.id.sw_activate_quitedroid,
                        "\n\nClick on the switch to activate QuietDroid",
                        false);
                break;
            case 1:
                tabHost.setCurrentTab(2);
                GetShowCaseView(((TabHost)activity.findViewById(R.id.tabHost)).getTabWidget().getChildTabViewAt(2),
                        "The events tab shows upcoming events for this week",
                        false);
                break;
            case 2:
                GetShowCaseView(R.id.lblRefreshEvents,
                                "If nothing is shown, there are no events coming up" +
                                "\n\nOtherwise click the refresh button and try again",
                        false);
                break;
            case 3:
                GetShowCaseView(R.id.legend,
                        "\n\n\n\nCalendar events have one of three modes" +
                        "\n\nTry clicking each mode to see what they mean",
                        false);
                break;
            case 4:
                GetShowCaseView(R.id.iconSettings,
                        "\n\n\n\nHow a calendar event is named will change its sound profile",
                        true);
                break;
            case 5:
                GetShowCaseView(R.id.iconSettings,
                        "Click the settings button to configure the keyword list",
                        false);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean(GlobalConstants.FIRST_BOOT_MAIN, false);
                editor.apply();
                break;
            case 6:
                //FINISH SHOW
//                SharedPreferences.Editor editor = sharedPreferences.edit();
//                editor.putBoolean(GlobalConstants.FIRST_BOOT, false);
                break;

        }
    }

}
