package jid.quitedroid.Database;

import android.provider.BaseColumns;

/**
 * Created by JiYeon on 2016-06-28.
 */
public class TableData {
    public static abstract class TableInfo implements BaseColumns
    {
        public static final String DATABASE_NAME = "quitedroid_info"; //Database Name

        /* Calendar Events */
        public static final String EVENT_TITLE = "event_title"; //First column name (0)
        public static final String EVENT_START_TIME = "event_start_time"; //Second column name (1)
        public static final String EVENT_END_TIME = "event_end_time"; //Third column name (2)
        public static final String EVENT_MODE = "event_mode"; //Fourth column name (3)
        public static final String GROUP_ID = "group_id"; //Fifth column name (4)
        public static final String EVENT_ID = "event_id"; //Sixth column name (5)
        public static final String EVENT_RRULE = "event_rrule"; //Seventh column name (6)
        public static final String TABLE_NAME_EXTRACTED_EVENTS = "calendar_info_raw"; //Table Name
        public static final String TABLE_NAME_SCHEDULED_EVENTS = "calendar_info_organized"; //Table Name
        public static final String TABLE_NAME_FORCED = "calendar_info_forced_events_from_quick_add"; //Table Name

        /* Table Names */
        public static final String TABLE_NAME_BLOCKED_CALL_LOG = "blocked_call_log"; 
        public static final String CALLER_NAME = "caller_name";
        public static final String CALLER_EVENT_TITLE = "event_title";
        public static final String CALLER_MODE = "mode";
        public static final String CALLER_TIME = "time";

        //Queries
        public static String CREATE_QUERY_RAW = "CREATE TABLE " + TABLE_NAME_EXTRACTED_EVENTS + "(" + EVENT_TITLE + " TEXT,"
                + EVENT_START_TIME + " LONG," + EVENT_END_TIME + " LONG," + EVENT_MODE +
                " INTEGER,"+ GROUP_ID + " INTEGER," + EVENT_ID + " TEXT," + EVENT_RRULE + " TEXT);";

        public static String CREATE_QUERY_ORGANIZED = "CREATE TABLE " + TABLE_NAME_SCHEDULED_EVENTS + "(" + EVENT_TITLE +
                " TEXT," + EVENT_START_TIME + " LONG," + EVENT_END_TIME + " LONG," +
                EVENT_MODE + " INTEGER," + GROUP_ID + " INTEGER," + EVENT_ID + " TEXT," + EVENT_RRULE + " TEXT);";

        public static String CREATE_QUERY_FORCED = "CREATE TABLE " + TABLE_NAME_FORCED + "(" + EVENT_TITLE +
                " TEXT," + EVENT_START_TIME + " LONG," + EVENT_END_TIME + " LONG," +
                EVENT_MODE + " INTEGER," + GROUP_ID + " INTEGER," + EVENT_ID + " TEXT," + EVENT_RRULE + " TEXT);";

        public static String CREATE_QUERY_CALL_LOG = "CREATE TABLE " + TABLE_NAME_BLOCKED_CALL_LOG + "(" +
                CALLER_NAME + " TEXT," + CALLER_EVENT_TITLE + " TEXT," + CALLER_MODE +
                " TEXT," + CALLER_TIME + " TEXT);";
    }
}
