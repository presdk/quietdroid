package jid.budgetlogger;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Dawin on 6/18/2016.
 */
public class InitFragment extends DialogFragment {

    private EditText txtEventName, txtTime;
    private Button btnSubmit;

    public InitFragment(){
        //Empty
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_init, container);
        txtEventName = (EditText)view.findViewById(R.id.txtEventName);
        txtEventName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) txtEventName.setText("", TextView.BufferType.EDITABLE);
            }
        });
        txtEventName.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.d("TEG", "onKey: " + keyCode);
                if(event.getKeyCode() == KeyEvent.KEYCODE_ENTER){
                    v.requestFocus(View.FOCUS_DOWN);
                    return true;
                }
                return false;
            }
        });

        txtTime = (EditText) view.findViewById(R.id.txtTime);
        txtTime.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) txtTime.setText("", TextView.BufferType.EDITABLE);
            }
        });

        getDialog().setTitle("Set Initial Event");

        Button btnSubmit = (Button)view.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stringToShow = txtTime.getText() + " " +
                        txtEventName.getText();
                Toast.makeText(getActivity().getApplicationContext(),
                        stringToShow,
                        Toast.LENGTH_LONG).show();


                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });
        return view;
    }


}
